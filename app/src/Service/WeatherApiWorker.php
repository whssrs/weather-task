<?php

namespace App\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use App\Entity\Weather;
use Monolog\Logger;
use Monolog\Handler\ErrorLogHandler;


class WeatherApiWorker
{
    const DEBUG = false;
    const TEMP_COUNT = 6;
    const MAX_RESULTS = 18; // 6 (values in hour) * 3 (hours)
    const DAY_RESULTS = 144; // 6 (values in hour) * 24 (hours)

    private $entityManager;

    public function __construct(EntityManagerInterface $em)
    {
        $this->entityManager = $em;
    }

    public function processData()
    {
        $url = getenv('APIXU_URL');
        $key = getenv('APIXU_KEY');
        $ip = getenv('APIXU_IP');

        $client = new Client(['base_uri' => $url]);
        try {
            $response = $client->request('get', '', [
                'query' => [
                    'key' => $key,
                    'q' => $ip
                ]
            ]);
        } catch (RequestException $ex) {
            $logger = new Logger("apixu log");
            $logger->pushHandler(new ErrorLogHandler());
            $logger->addInfo($ex->getMessage());
            return;
        }

        if ($response->getStatusCode() == 200) {
            $body = json_decode($response->getBody());
            $temperature = $body->current->temp_c;
            $weather = new Weather();
            $weather->setValues($temperature, new \DateTime());
            $this->entityManager->persist($weather);
            $this->entityManager->flush();
            $this->predictValues();
        }
    }

    public function predictValues()
    {
        $cache = new FilesystemAdapter("app.cache");
        $weatherCacheItem = $cache->getItem('weather');

        $repository = $this->entityManager->getRepository(Weather::Class);
        $weatherArray = array_reverse($repository->findLastResults(self::MAX_RESULTS));

        if (count($weatherArray) < self::TEMP_COUNT) {
            $cache->deleteItem('weather');
            return;
        }

        $temperatures = array_map(
            function (Weather $w) {
                return $w->getTemperature();
            },
            $weatherArray
        );

        $timeArray = array_map(
            function (Weather $w) {
                return clone($w->getTime());
            },
            $weatherArray
        );

        //check serious cron fails
        $wrongInterval = false;
        foreach ($timeArray as $key => $value) {
            if (($key > 0) && ($value->getTimestamp() - $timeArray[$key - 1]->getTimestamp() > 900)) {
                $wrongInterval = true;
                break;
            }
        }

        $currentTime = new \DateTime();

        //Predict temperatures
        for ($i = 0; $i < self::TEMP_COUNT; $i++) {
            $newValue = \trader_tsf($temperatures, count($temperatures));
            $newValue = array_pop($newValue);
            $currentTime = $currentTime->modify('10 minutes');
            array_push($timeArray, clone($currentTime));
            array_push($temperatures, $newValue);
        }

        // Show results only for 2 hours
        $weatherCacheItem->set([
            'temperature' => array_slice($temperatures, -2 * self::TEMP_COUNT),
            'time' => array_slice($timeArray, -2 * self::TEMP_COUNT),
            'wrong_interval' => $wrongInterval
        ]);

        $cache->save($weatherCacheItem);

    }
}
