<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;

class WeatherController extends AbstractController
{
    /**
     * @Route("/weather", name="weather")
     */
    public function index()
    {
        $cache = new FilesystemAdapter("app.cache");
        $weatherCacheItem = $cache->getItem('weather');

        if (!$weatherCacheItem->isHit()) {
            return $this->render('weather/error.html.twig', [
                'message' => 'Недостаточно данных. Попробуйте повторить попытку позже'
            ]);
        }

        $weather = $weatherCacheItem->get();

        $warning = "";
        foreach ($weather['time'] as &$value) {
            $value = $value->format("H:i");
        }

        return $this->render('weather/index.html.twig', [
            'values' => $weather['temperature'],
            'labels' => $weather['time'],
            'wrong_interval' => $weather['wrong_interval']
        ]);
    }


    /**
     * GET /bound
     * Получает нижнюю границу температуры за текущий день
     *
     * @Route("/bound", name="getBound")
     *
     * @return JsonResponse
     *
     * @throws HttpException
     *
     */
    public function getBound()
    {
        throw new HttpException(500, 'Something went wrong');

        return new JsonResponse(['ab' => 123]);
    }

}
