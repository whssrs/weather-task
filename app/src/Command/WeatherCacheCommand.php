<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Service\WeatherApiWorker;

class WeatherCacheCommand extends Command
{
    protected static $defaultName = 'app:cache:rebuild';
    protected $service;

    public function __construct(WeatherApiWorker $worker)
    {
        parent::__construct();
        $this->service = $worker;
    }

    protected function configure()
    {
        $this->setDescription('Rebuild weather cache');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->service->predictValues();
    }
}
