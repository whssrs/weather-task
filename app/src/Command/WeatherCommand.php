<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Service\WeatherApiWorker;

class WeatherCommand extends Command
{
    protected static $defaultName = 'app:get-weather';
    protected $service;

    public function __construct(WeatherApiWorker $worker)
    {
        parent::__construct();
        $this->service = $worker;
    }

    protected function configure()
    {
        $this->setDescription('Gets weather from Apixu.com');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->service->processData();
    }
}
